// SPDX-License-Identifier: GPL-2.0+

#include <asm/io.h>
#include <asm-generic/gpio.h>

#define PMU_BASE_ADDR	0xfdd90000
#define PMU_PWR_CON	(0x04)

int rk_board_late_init(void)
{
	/* set pmu_sleep_pol to low */
	writel(0x80008000, PMU_BASE_ADDR + PMU_PWR_CON);

	return 0;
}

void led_setup(void)
{
	int ret;
	ret = gpio_request(117, "charging_led");
	if (ret) {
		printf("%s: failed to get charging_led %d\n", __func__, ret);
		return;
	}
	gpio_direction_output(27, 1);
	if (ret) {
		printf("%s: failed to set charging_led %d\n", __func__, ret);
		return;
	}
	return;
}

